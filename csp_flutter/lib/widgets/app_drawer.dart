import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/providers/user_provider.dart';

class AppDrawer extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        

        return Drawer(
            child: Container(
                width: double.infinity,
                color: Color.fromRGBO(20, 45, 68, 1),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        DrawerHeader(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    SizedBox(height: 36.0),
                                    Image.asset(
                                        'assets/ffuf-logo.png',
                                        color: Colors.white,
                                        width: 100
                                    ), 
                                    Container(
                                        margin: EdgeInsets.only(top: 16.0),
                                        child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                Text('FFUF Project Management', style: TextStyle(fontSize: 20.0, color: Colors.white)),
                                                Text('Made by FFUF Internal Dev Team', style: TextStyle(color: Colors.white)),
                                            ]
                                        )
                                    )
                                ]
                            )
                        ),
                        Spacer(),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                
                                 ListTile(
                                    title: Text('About me', style: TextStyle(color: Colors.white)),
                                    onTap: () async {
                                        Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                        Navigator.pushNamed(context, '/about-screen');
                                    }
                                ),
                                ListTile(
                                    title: Text('About company', style: TextStyle(color: Colors.white)),
                                    onTap: () async {
                                        Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                        Navigator.pushNamed(context, '/about-company-screen');
                                    }
                                ),
                                ListTile(
                                    title: Text('Logout', style: TextStyle(color: Colors.white)),
                                    onTap: () async {
                                        Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                        Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
                                    }
                                )
                            ]
                        )
                    ]
                )
            )
        );
    }
}