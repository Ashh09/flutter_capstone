import 'dart:ui';
import 'package:flutter/material.dart';

class AboutCompanyScreen extends StatelessWidget {

    final divider = new Divider(color: Colors.black);
    @override
    Widget build(BuildContext context) {
    
    Widget companyName = Container( 
        width: double.infinity,
        margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
        child: Column(
            children: [
                Text(
                    'FFUF Manila',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 40,
                    )
                ),
                Text(
                    '#Mabuhay',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                     )
                ),
            ]
        )
    );
    Widget companyIntroduction = Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [ 
            Expanded(
                child: Container( 
                    padding: EdgeInsets.only(top: 18.0, bottom:18.0, left: 24.0, right: 24.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(
                                "The metropolitan region of Manila is the political and economic center of the Philippines and, once you get to know it properly, a metropolis with great charm.\n",
                                style: TextStyle(
                                    fontSize: 17,
                                )
                            ),
                            Text(
                                "After work, the hits of past decades can be heard from the nearby karaoke bar.",
                                style: TextStyle(
                                    fontSize: 17,
                                )
                            ),
                        ]
                    )
                )
            ),
        ]
    );
    Widget aboutCompany = Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [ 
            Expanded(
                child: Container( 
                    padding: EdgeInsets.only(top: 18.0, bottom:18.0, left: 24.0, right: 24.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(
                                "FFUF Manila allows us to scale\n",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                )
                            ),
                            Text(
                                "We founded FFUF Manila for continuous growth and the answer to scaling issues in times of a shortage of skilled workers. While Feil, Feil & Feil in Germany has grown in personnel by around 20% per year in recent years, the number of our Filipino colleagues has doubled year after year. Almost 40 FFUF Manila employees are now working on software components that are planned and assembled by our architects in Ludwigsburg.",
                                style: TextStyle(
                                    fontSize: 17,
                                )
                            ),
                        ]
                    )
                )
            ),
        ]
    );
    
    Widget contactInfo = Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [ 
            Expanded(
                child: Container( 
                    padding: EdgeInsets.only(top: 18.0, bottom:18.0, left: 24.0, right: 24.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(
                                "FFUF Manila\n",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                )
                            ),
                            Text(
                                "FFUF Manila Inc.\nPhilippines\n6783 Ayala Ave\n1200 Metro Manila\n\n+63 915 731 6140\nmanila@ffuf.de",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17,
                                )
                            ),
                        ]
                    )
                )
            ),
        ]
    );
    Widget lblAppTitles = Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
            child: Column(
                children: [
                    Image.asset('assets/ffuf-logo.png', width: 300),
                    companyName,
                    companyIntroduction,
                    divider,
                    aboutCompany,
                    divider,   
                    contactInfo
                ]
            )    
        );
    return Scaffold(
        appBar: AppBar(
                    centerTitle: false,
                    title: Text('About Company'),
                    backgroundColor: Color.fromRGBO(20, 45, 68, 1),
                ),
        body: SingleChildScrollView( 
            child:Container(
                width: double.infinity,
                child: Container(decoration: BoxDecoration(      
                ), 
                child: lblAppTitles)
            )
        )
    );
    }
}