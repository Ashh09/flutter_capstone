import 'dart:async';

import '/models/user.dart';

import 'package:csp_flutter/utils/api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '/providers/user_provider.dart';

import '/utils/functions.dart';

class LoginScreen extends StatefulWidget {
    @override
    _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {

    Future<User>? _futureLogin;
    final _formKey = GlobalKey<FormState>();
    final _tffEmailController = TextEditingController();
    final _tffPasswordController = TextEditingController();
    
    void _login(BuildContext context){
        setState((){
            _futureLogin = API().login(
                email: _tffEmailController.text, 
                password: _tffPasswordController.text
            ).catchError((error){
                showSnackBar(context, error.message);
            });
        });
    }


    void _goToHomepage(data) async {
        User user = data as User;

        final prefs = await SharedPreferences.getInstance();
        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setDesignation = Provider.of<UserProvider>(context, listen: false).setDesignation;
        
        if (user.accessToken != null) {
            setAccessToken(user.accessToken);
            setDesignation(user.designation);

            prefs.setString('accessToken', user.accessToken!);
            prefs.setString('designation', user.designation!);

            Navigator.pushReplacementNamed(context, '/project-list');
        } else {
            
            Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
            showSnackBar(context, 'User not found.');
        }
    }

    @override
    Widget build(BuildContext context) {

        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setUserId = Provider.of<UserProvider>(context, listen: false).setUserId;

        Widget txtEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid email.';
            }
        );

        Widget txtPassword = TextFormField(
            decoration: InputDecoration(labelText:'Password'),
            obscureText: true,
            controller: _tffPasswordController,
            validator: (value){
                bool isValid = value != null && value.isNotEmpty;
                return (isValid) ? null : 'The password must be provided.';
            }
        );

        Widget btnSubmit = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 10.0),
            child: ElevatedButton(
                onPressed: () { 
                    if (_formKey.currentState!.validate()) {
                        _login(context);

                    } else {
                       return showSnackBar(context, 'Form validation failed. Check input and try again.');
                    }
                }, 
                child: Text('Login')
            )
        );

        
        Widget lblAppTitle = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 60.0, bottom: 60.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    // Add styles to the texts below (size 30, bold font weight, red color on Login text).
                    Text(
                        'Project Management',
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold
                        )
                    ),
                    Text(
                        'Login',
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: Colors.red
                        )
                    )
                ]
            )
        );

        Widget imgLogo = Expanded(
            child: Padding(
                padding: EdgeInsets.only(bottom: 16.0),
                child: Align(
                    alignment: Alignment.bottomCenter,
                    // Aligning of image.
                    child: Image.asset(
                        'assets/ffuf-logo.png',
                         width: 100
                    )
                )
            )
        );

        Widget formLogin = Form(
            key: _formKey,
            child: Column(
                children: [
                    lblAppTitle,
                    txtEmail,
                    txtPassword,
                    btnSubmit,
                    imgLogo
                ]
            )
        );

        Widget loginView = FutureBuilder(
            future: _futureLogin,
            builder: (context, snapshot) {
                if (_futureLogin == null) {
                    return formLogin;
                } else if (snapshot.hasError == true) {
                    return formLogin;
                } else if (snapshot.hasData == true) {
                    Timer(Duration(milliseconds: 1), ()  {
                        User user = snapshot.data as User;
                        setUserId(user.id);
                        setAccessToken(user.accessToken);
                        _goToHomepage(snapshot.data);
                    });

                    return Container();
                }
                    return Center(
                    child: CircularProgressIndicator()
                    );
            }
        );

        return Scaffold(
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 30.0, left: 36.0, right: 36.0),
                child: loginView
            )
        );
    }
}