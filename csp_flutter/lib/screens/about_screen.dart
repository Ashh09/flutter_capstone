import 'package:csp_flutter/widgets/app_drawer.dart';
import 'package:flutter/material.dart';


class AboutScreen extends StatelessWidget {

    final divider = new Divider(color: Colors.black);
    @override
    Widget build(BuildContext context) {

        Widget devImage() {
            return ClipOval(
                child: Material(                    
                    color: Colors.transparent,
                    child: Ink.image(
                        image: Image.asset('assets/alyssa.jpeg').image,
                        fit: BoxFit.cover,
                        width: 200,
                        height: 200,
                    ),
                ),
            );
        }

        Widget devBasicInfo = Column(
            children: [
                Text(
                    "Alyssa Ashley C.Mendoza",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                ),
                
                Text('Flutter Development Trainee', 
                    style: TextStyle(
                        fontSize: 15,
                    )
                ),
                Text('mendozaaac09@gmail.com\n' 
                    'www.linkedin.com/in/mendozaaac',
                    style: TextStyle(
                        fontSize: 14,
                        decoration: TextDecoration.underline,
                        color: Colors.black,
                    ),
                ),
            ],
        );
    

        Widget devAboutInfo = Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Expanded(
                    child: Container( 
                        padding: EdgeInsets.only( top: 24.0, left: 24.0, right: 24.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                                Text('About',
                                    style: TextStyle(
                                        fontSize: 20, 
                                        fontWeight: FontWeight.bold
                                    ),
                                ),
                                const SizedBox(height: 8),

                                Text(
                                    "   My name is Alyssa Ashley Mendoza, you can call me Ash or Ashley. "
                                    "I am a currently a flutter developer trainee at FFUF Manila Inc. "
                                    "I am also a Mountaineer, volunteer and marshall. "
                                    "We have an adopted Aeta community located in Porac,Pampanga. "
                                    "My hobbies are solving puzzles like the rubiks cube, megaminx,pyraminx etc. ",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                    )
                                )
                            ]
                        )
            
                    ) 
                ),
            ]
        );

        Widget devWorkExperience = Row(

            crossAxisAlignment: CrossAxisAlignment.start,
                children: [ 
                    Expanded(child: Container( 
                        padding: EdgeInsets.only( top: 15, left: 24.0, right: 24.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Text('Prior Work Experience', style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold
                                )),
                                Text(
                                    "   • BA Documentation & Quality\n     Assurance PurpleBug Inc.",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18
                                    )
                                ),
                                Text(
                                    "   • System Administrator - Isla Medical\n     Services Phillipines Inc.",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18
                                    )
                                ),
                            ]
                        )
                    )
                ),
            ]
        );

        Widget devSkills = Row(

            crossAxisAlignment: CrossAxisAlignment.start,
                children: [ 
                    Expanded(child: Container( 
                        padding: EdgeInsets.only( top: 15, left: 24.0, right: 24.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Text('Skills', style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold
                                )),
                                Text(
                                    "   • Programming\n"
                                    "   • Dart\n"
                                    "   • Flutter\n"
                                    "   • JavaScript\n"
                                    "   • Java\n"
                                    "   • Mobile Development\n"
                                    "   • Software Development\n"
                                    "   • Git\n"
                                    "   • Odoo Development\n",            
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18
                                    )
                                ),
                            ]
                        )
                    )
                ),
            ]
        );

        Widget devInterest = Row(

            crossAxisAlignment: CrossAxisAlignment.start,
                children: [ 
                    Expanded(child: Container( 
                        padding: EdgeInsets.only( top: 15, left: 24.0, right: 24.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Text('Interest', style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold
                                )),
                                Text(
                                    "   • Mountaineering\n"
                                    "   • Puzzles\n"
                                    "   • Sports\n"
                                    "   • Gaming\n"
                                    "   • Voluteering\n"
                                    "   • Travelling\n",            
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18
                                    )
                                ),
                            ]
                        )
                    )
                ),
            ]
        );

        return Scaffold(
                appBar: AppBar(
                    centerTitle: false,
                    title: Text('Developer Page'),
                    backgroundColor: Color.fromRGBO(20, 45, 68, 1),
                ),
                
                endDrawer: AppDrawer(),
                body: SingleChildScrollView(
                    child:Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/bg.jpeg"),
                                fit: BoxFit.cover,
                            )
                        ),
                        width: double.infinity,
                        padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                        child: Column(
                            children: <Widget>[
                                devImage(),
                                devBasicInfo, 
                                devAboutInfo,
                                divider,
                                devWorkExperience,
                                divider,
                                devSkills,
                                divider,
                                devInterest
                            ],
                        )
                    
                    ),
                )
        );
    }
}
