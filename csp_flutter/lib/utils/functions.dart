import 'package:flutter/material.dart';

//snackbar function to call when snackbar is needed.
void showSnackBar(BuildContext context, String message) {
    SnackBar snackBar = new SnackBar(
        content: Text(message), 
        duration: Duration(milliseconds: 2000)
    );
    
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
}