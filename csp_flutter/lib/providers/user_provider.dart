import 'package:flutter/foundation.dart';

class UserProvider extends ChangeNotifier {
    String? _accessToken;
    String? _designation;
    int? _userId;

    UserProvider([ this._accessToken, this._designation ]);

    get accessToken => _accessToken;
    get userId => _userId;
    get designation => _designation;


    setAccessToken(String? value) {
        _accessToken = value;
        notifyListeners();
    }

    setUserId(int? value) {
        _userId = value;
        notifyListeners();
    }
    setDesignation(String? value) {
        _designation = value;
        notifyListeners();
    }
}