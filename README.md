# Project Management App

# About The Project

---

This project is our capstone project where we were asked to create an app that has:

- Login and authenticator.
- About screen to see the details of the app's developer page.
- Logout in the app's drawer to go back to the login screen.

## Features

### Contractor

- Add new projects.
- Assign projects into sub-contractors.
- View task lists and view the details of every tasks.

### Subcontractor

- Create tasks for the projects assigned to it
- Assign tasks to the assembly-team.
- View tasks it creates and view the details of its tasks.

### Assembly-team

- View tasks assigned to him.
- Start and finish a project.

# Software Requirements

---

- [VS Code](https://code.visualstudio.com/download)
- iOS and Android Emulator ([Xcode](https://developer.apple.com/xcode/resources/) or [Android Studio](https://developer.android.com/studio))
- [Flutter](https://flutter.dev/docs/get-started/install) (version 2.2.3)

---

# Set-Up Guide
## Project Cloning

To open the system this is the link of the bitbucket repository

Link: [https://bitbucket.org/Ashh09/flutter_capstone/src/master/](https://bitbucket.org/Ashh09/flutter_capstone/src/master/)

- Open terminal
- From a terminal window, change into the local directory where you want to clone your repository.

    ```jsx
     cd <path_to_directory>
    ```

- Clone the repository

    ```jsx
    git clone <project_url>
    ```

- If the clone was successful, a new sub-directory appears on your local drive with the same name as the repository that you cloned.

## Package Installation

- Open the cloned app in VS Code.
- Open the `pubspec.yaml` file and save it.
- If you are going to add new packages, add the corresponding package under dependencies (you must check for proper indentions.)
- Install the added packages by opening the terminal.
- If you are not in the directory of your cloned application, you can change the directory by typing this into your terminal.

    ```jsx
     cd <path_to_directory>
    ```

- If you are in the right directory path run this command in your terminal.

    ```jsx
    flutter pub get
    ```

## Running The App

### Step 1

Run the API

- Open terminal
- From a terminal window, change into the local directory to `csp_api`

    ```jsx
    cd csp_api
    ```

- Run the API by typing this in your terminal

    ```jsx
    dart run bin/main.dart
    ```

### Step 2

Run the main app

- Go to `main.dart`
- click `debug` and wait for the app to open.

---

# Test User Credentials
| Email  | Password |
| ------------- | ------------- |
| contractor@gmail.com  | contractor  |
| subcontractor@gmail.com  | subcontractor  |
| assembly-team@gmail.com  | assembly-team  |

---
# Packages

1. [image_picker:](https://pub.dev/packages/image_picker) ^0.8.0+3
    - A Flutter plugin for iOS and Android for picking images from the image library, and taking new pictures with the camera.
2. [flutter_dotenv](https://pub.dev/packages/flutter_dotenv): ^5.0.0
    - Load configuration at runtime from a .env file which can be used throughout the application.
3. [http](https://pub.dev/packages/http): ^0.13.3
    - This package contains a set of high-level functions and classes that make it easy to consume HTTP resources. It's multi-platform, and supports mobile, desktop, and the browser.
4. [provider](https://pub.dev/packages/provider): ^5.0.0
    - A wrapper around InheritedWidget to make them easier to use and more reusable.
5. [shared_preferences](https://pub.dev/packages/shared_preferences): ^2.0.6
    - Wraps platform-specific persistent storage for simple data (NSUserDefaults on iOS and macOS, SharedPreferences on Android, etc.). Data may be persisted to disk asynchronously, and there is no guarantee that writes will be persisted to disk after returning, so this plugin must not be used for storing critical data.
6. [url_launcher](https://pub.dev/packages/url_launcher): ^6.0.9
    - A Flutter plugin for launching a URL. Supports iOS, Android, web, Windows, macOS, and Linux.